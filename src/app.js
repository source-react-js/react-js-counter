import './app.css'
import Hook from './Counter/hook.jsx'
import Class from './Counter/class.jsx'
export const App = () => {
  return (
    <div className="app">
      <Hook/>
      <Class/>
    </div>
  )
}
