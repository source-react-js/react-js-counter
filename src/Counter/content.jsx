import logo from './logo.svg';
import './style.css'

const content = (props) => {
  const { number, value, clickAdd, clickDelete, changeValue, type } = props
  return (
    <div className='counter'>
      <div className='number'>
        <div>
          <img src={logo} className="logo" alt="logo" />
        </div>
        <div>
          Number <label className='text'>[ {number} ]</label>
        </div>
        <div>
          <img src={logo} className="logo" alt="logo" />
        </div>
      </div>
      <div className='btn-set-number'>
        <div className='btn' onClick={clickAdd}> + </div>
        <div className='btn' onClick={clickDelete}> - </div>
      </div>
      Set value calculate of number :
      <input className='input' value={value} onChange={changeValue} maxLength='10' />
      <div className='description'>
        This Content create code by {type}.
      </div>
    </div>
  )
}

export default content