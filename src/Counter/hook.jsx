import { useState } from "react"
import Content  from "./content"
export const Counter = () => {

  const [number, setNumber] = useState(0)
  const [value, setValue] = useState(1)

  const clickAdd = () => {
    setNumber(number + value)
  }

  const clickDelete = () => {
    setNumber(number - value)
  }

  const changeValue = (element) => {
    if(!isNaN(element.target.value)){ //only number
      setValue(+element.target.value)
    }
  }

  return (
    <Content number={number} value={value} clickAdd={clickAdd} clickDelete={clickDelete} changeValue={changeValue} type='Hook'/>
  )
}

export default Counter