import { Component } from "react"
import Content from './content'
export class Counter extends Component {
  constructor() {
    super()
    this.state = {
      number: 0,
      value: 1
    }
  }

  clickAdd = () => {
    const { number, value } = this.state

    this.setState({
      number: number + value
    })
  }

  clickDelete = () => {
    const { number, value } = this.state
    this.setState({
      number: number - value
    })
  }

  changeValue = (element) => {
    if (!isNaN(element.target.value)) { //only number
      this.setState({
        value: +element.target.value
      })
    }
  }

  render() {
    const { number, value } = this.state
    return (
      <Content number={number} value={value} clickAdd={this.clickAdd} clickDelete={this.clickDelete} changeValue={this.changeValue} type='Class'/>
    )
  }
}

export default Counter
